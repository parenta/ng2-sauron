#addin "Cake.Npm"
#addin "Cake.Json"
#addin "Cake.Git"

var target = Argument("target", "Default");

Setup(context =>
{
    Information("███╗   ██╗ ██████╗ ██████╗       ███████╗ █████╗ ██╗   ██╗██████╗  ██████╗ ███╗   ██╗");
    Information("████╗  ██║██╔════╝ ╚════██╗      ██╔════╝██╔══██╗██║   ██║██╔══██╗██╔═══██╗████╗  ██║");
    Information("██╔██╗ ██║██║  ███╗ █████╔╝█████╗███████╗███████║██║   ██║██████╔╝██║   ██║██╔██╗ ██║");
    Information("██║╚██╗██║██║   ██║██╔═══╝ ╚════╝╚════██║██╔══██║██║   ██║██╔══██╗██║   ██║██║╚██╗██║");
    Information("██║ ╚████║╚██████╔╝███████╗      ███████║██║  ██║╚██████╔╝██║  ██║╚██████╔╝██║ ╚████║");
    Information("╚═╝  ╚═══╝ ╚═════╝ ╚══════╝      ╚══════╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝");
});

Task("__Versioning")
    .Does(() => {
		TeamCity.WriteStartBlock("Build number versioning");
        
		var packageFile = "./package.json";
        
        var deserializedPackage = ParseJsonFromFile(packageFile);
		TeamCity.SetBuildNumber(deserializedPackage["version"].ToString());		

        TeamCity.WriteEndBlock("Build number versioning"); 
	});

Task("__NpmInstall")    
    .Does(() => {
        TeamCity.WriteStartBlock("Npm Install");
        Npm.Install();
        TeamCity.WriteEndBlock("Npm Install"); 
    });

Task("__Build")    
    .Does(() => {
        TeamCity.WriteStartBlock("Build");
        Npm.RunScript("prepublish");
        TeamCity.WriteEndBlock("Build");
    });

Task("__Test")    
    .Does(() => {
        TeamCity.WriteStartBlock("Test");
        Npm.RunScript("test");
        TeamCity.WriteEndBlock("Test");
    });

Task("__Release")
     .Does(() => {
        TeamCity.WriteStartBlock("Commit Release");        
        Npm.RunScript("release");
        TeamCity.WriteEndBlock("Commit Release");
     });



Task("Build")
    .IsDependentOn("__Versioning")
    .IsDependentOn("__NpmInstall")
    .IsDependentOn("__Build")
    .IsDependentOn("__Test")
    .IsDependentOn("__Release");


Task("Default")
  .IsDependentOn("Build");

RunTarget(target);