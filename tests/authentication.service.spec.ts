import { Injector } from '@angular/core';
import { XHRBackend, HttpModule } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { SauronModule } from '../ng2-sauron.module';
import { getTestBed, TestBed } from '@angular/core/testing';
import { LocalStorageService } from 'angular-2-local-storage';
import { ISauronClientConfig } from '../src/config/isauron-client-config';
import { SAURON_CLIENT_CONFIG } from '../src/config/sauron-client-config';
import {AuthenticationService} from '../src/authentication.service';

class MockLocalStorageService {

}

describe('Service: Authentication', () => {
    let injector: Injector;
    let backend: MockBackend;
    let translate: AuthenticationService;
    let connection: MockConnection; // this will be set when a new connection is emitted from the backend.

    let sauronClientConfig: ISauronClientConfig = {
        apiUrl: 'http://localhost/api/',
        apiKey: '5df1681c-ee6a-405f-8895-34570cd3d23b',
        errorFunc: (error: any) => {
            // DO SOMETHING WITH THIS
            console.log(error);
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule, SauronModule ],
            providers: [
                {provide: XHRBackend, useClass: MockBackend},
                {provide: LocalStorageService, useClass: MockLocalStorageService},
                {provide: AuthenticationService, useClass: AuthenticationService},
                {provide: SAURON_CLIENT_CONFIG, useValue: sauronClientConfig},
            ]
        });
        injector = getTestBed();
        backend = injector.get(XHRBackend);
        translate = injector.get(AuthenticationService);
        // sets the connection when someone tries to access the backend with an xhr request
        backend.connections.subscribe((c: MockConnection) => connection = c);
    });

    afterEach(() => {
        injector = undefined;
        backend = undefined;
        translate = undefined;
        connection = undefined;
    });

    it('is defined', () => {
        expect(AuthenticationService).toBeDefined();
        expect(translate).toBeDefined();
        expect(translate instanceof AuthenticationService).toBeTruthy();
    });

});
