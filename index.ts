export * from './src/config/isauron-client-config';
export * from './src/config/sauron-client-config';

import * as commandresults from './src/entities/commandresults/command-results';
export { commandresults as CommandResultEntities  };

import * as commands from './src/entities/commands/commands';
export { commands as CommandEntities  };

import * as queries from './src/entities/queries/queries';
export { queries as QueryEntities  };

import * as queryServices from './src/query/query.service';
export {queryServices as QueryServices}

export * from './src/types/guid'

export * from './src/execution.service';

export * from './src/authentication.service';

export * from './ng2-sauron.module'
