import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { AuthenticationService } from './src/authentication.service';

@NgModule({
    imports: [
        CommonModule,
        HttpModule
    ],
    providers: [AuthenticationService],
})
export class SauronModule {

}
