export abstract class BaseCommand {
    constructor(private route: string) {
    }

    getRoute(): string {
        return this.route;
    }
}
