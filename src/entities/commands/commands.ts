﻿import { BaseCommand } from './base-command';
import { Guid } from '../../types/guid';
import * as moment from 'moment';
export namespace ViewedSystemTourCommand {

    export class UserViewedWelcomeTourCommand extends BaseCommand {
        constructor() {
            super('ViewedSystemTour/UserViewedWelcomeTourCommand');
        }
    }
}
export namespace UserCommand {

    export class CreateBasicUserCommand extends BaseCommand {
        businessId: Guid;
        firstName: string;
        lastName: string;
        legacyId: number;
        emailAddress: string;
        constructor() {
            super('User/CreateBasicUserCommand');
        }
    }

    export class CreateNotificationForUserCommand extends BaseCommand {
        message: string;
        action: string;
        actionType: any; // ActionType;
        notificationType: any; // NotificationType;
        userId: Guid;
        constructor() {
            super('User/CreateNotificationForUserCommand');
        }
    }

    export class MarkAllNotificationsForUserAsReadCommand extends BaseCommand {
        constructor() {
            super('User/MarkAllNotificationsForUserAsReadCommand');
        }
    }

    export class MarkNotificationAsReadCommand extends BaseCommand {
        notificationId: Guid;
        constructor() {
            super('User/MarkNotificationAsReadCommand');
        }
    }

    export class MarkSystemAlertAsClosedCommand extends BaseCommand {
        userSystemAlertId: Guid;
        constructor() {
            super('User/MarkSystemAlertAsClosedCommand');
        }
    }

    export class MarkSystemAlertAsSeenCommand extends BaseCommand {
        userSystemAlertId: Guid;
        constructor() {
            super('User/MarkSystemAlertAsSeenCommand');
        }
    }

    export class UpdateLegacyIdCommand extends BaseCommand {
        legacyId: number;
        userId: Guid;
        constructor() {
            super('User/UpdateLegacyIdCommand');
        }
    }
}
export namespace UserPreferenceCommand {

    export class CreateDiarySettingsUserPreferenceCommand extends BaseCommand {
        data: string;
        constructor() {
            super('UserPreference/CreateDiarySettingsUserPreferenceCommand');
        }
    }

    export class CreateDashboardLayoutUserPreferenceCommand extends BaseCommand {
        data: string;
        constructor() {
            super('UserPreference/CreateDashboardLayoutUserPreferenceCommand');
        }
    }

    export class UpdateDiarySettingsUserPreferenceCommand extends BaseCommand {
        userPreferenceId: Guid;
        data: string;
        constructor() {
            super('UserPreference/UpdateDiarySettingsUserPreferenceCommand');
        }
    }

    export class UpdateDashboardLayoutUserPreferenceCommand extends BaseCommand {
        userPreferenceId: Guid;
        data: string;
        constructor() {
            super('UserPreference/UpdateDashboardLayoutUserPreferenceCommand');
        }
    }
}
export namespace UserIntegrationCommand {

    export class CreateCalendarEndpointCommand extends BaseCommand {
        userId: Guid;
        constructor() {
            super('UserIntegration/CreateCalendarEndpointCommand');
        }
    }
}
export namespace SystemMessageCommand {

    export class CreateGlobalSystemMessageCommand extends BaseCommand {
        message: string;
        colourType: any; // ColourType;
        constructor() {
            super('SystemMessage/CreateGlobalSystemMessageCommand');
        }
    }

    export class CreateMultiBusinessSystemMessageCommand extends BaseCommand {
        message: string;
        colourType: any; // ColourType;
        businessIds: any; // ICollection`1;
        constructor() {
            super('SystemMessage/CreateMultiBusinessSystemMessageCommand');
        }
    }

    export class CreateSingleBusinessSystemMessageCommand extends BaseCommand {
        message: string;
        colourType: any; // ColourType;
        businessId: Guid;
        constructor() {
            super('SystemMessage/CreateSingleBusinessSystemMessageCommand');
        }
    }

    export class CreateSingleUserSystemMessageCommand extends BaseCommand {
        message: string;
        colourType: any; // ColourType;
        userId: Guid;
        constructor() {
            super('SystemMessage/CreateSingleUserSystemMessageCommand');
        }
    }
}
export namespace SystemAlertCommand {

    export class CreateGlobalSystemAlertCommand extends BaseCommand {
        subject: string;
        message: string;
        colourType: any; // ColourType;
        isClosable: boolean;
        constructor() {
            super('SystemAlert/CreateGlobalSystemAlertCommand');
        }
    }

    export class CreateMultiBusinessSystemAlertCommand extends BaseCommand {
        subject: string;
        message: string;
        colourType: any; // ColourType;
        isClosable: boolean;
        businessIds: any; // ICollection`1;
        constructor() {
            super('SystemAlert/CreateMultiBusinessSystemAlertCommand');
        }
    }

    export class CreateSingleUserSystemAlertCommand extends BaseCommand {
        message: string;
        subject: string;
        colourType: any; // ColourType;
        isClosable: boolean;
        userId: Guid;
        constructor() {
            super('SystemAlert/CreateSingleUserSystemAlertCommand');
        }
    }

    export class CreateSingleBusinessSystemAlertCommand extends BaseCommand {
        message: string;
        subject: string;
        colourType: any; // ColourType;
        isClosable: boolean;
        businessId: Guid;
        constructor() {
            super('SystemAlert/CreateSingleBusinessSystemAlertCommand');
        }
    }

    export class RemoveSystemAlertCommand extends BaseCommand {
        systemAlertId: Guid;
        constructor() {
            super('SystemAlert/RemoveSystemAlertCommand');
        }
    }
}
export namespace ParentaStaffUserCommand {

    export class ChangeBasicDetailsCommand extends BaseCommand {
        firstName: string;
        lastName: string;
        constructor() {
            super('ParentaStaffUser/ChangeBasicDetailsCommand');
        }
    }

    export class ChangeEmailCommand extends BaseCommand {
        oldEmailAddress: string;
        newEmailAddress: string;
        constructor() {
            super('ParentaStaffUser/ChangeEmailCommand');
        }
    }

    export class ChangePasswordCommand extends BaseCommand {
        oldPassword: string;
        newPassword: string;
        constructor() {
            super('ParentaStaffUser/ChangePasswordCommand');
        }
    }

    export class CreateParentaStaffUserCommand extends BaseCommand {
        firstName: string;
        lastName: string;
        emailAddress: string;
        password: string;
        constructor() {
            super('ParentaStaffUser/CreateParentaStaffUserCommand');
        }
    }

    export class OverrideBasicDetailsCommand extends BaseCommand {
        firstName: string;
        lastName: string;
        userId: Guid;
        constructor() {
            super('ParentaStaffUser/OverrideBasicDetailsCommand');
        }
    }

    export class OverrideEmailAddressCommand extends BaseCommand {
        newEmailAddress: string;
        userId: Guid;
        constructor() {
            super('ParentaStaffUser/OverrideEmailAddressCommand');
        }
    }

    export class OverridePasswordCommand extends BaseCommand {
        parentaStaffUserId: Guid;
        newPassword: string;
        constructor() {
            super('ParentaStaffUser/OverridePasswordCommand');
        }
    }

    export class ResetPasswordCommand extends BaseCommand {
        token: string;
        newPassword: string;
        constructor() {
            super('ParentaStaffUser/ResetPasswordCommand');
        }
    }

    export class SendPasswordResetTokenCommand extends BaseCommand {
        emailAddress: string;
        constructor() {
            super('ParentaStaffUser/SendPasswordResetTokenCommand');
        }
    }

    export class UpdateProfileImageUrlCommand extends BaseCommand {
        parentaStaffUserId: Guid;
        profileImageUrl: string;
        constructor() {
            super('ParentaStaffUser/UpdateProfileImageUrlCommand');
        }
    }
}
export namespace ParentaStaffUserIntegrationCommand {

    export class UpdateBreatheHrIntegrationCommand extends BaseCommand {
        parentaStaffUserId: Guid;
        employeeRef: string;
        constructor() {
            super('ParentaStaffUserIntegration/UpdateBreatheHrIntegrationCommand');
        }
    }

    export class CreateBreatheHrIntegrationCommand extends BaseCommand {
        parentaStaffUserId: Guid;
        employeeRef: string;
        constructor() {
            super('ParentaStaffUserIntegration/CreateBreatheHrIntegrationCommand');
        }
    }

    export class CreateFootstepsLoginIntegrationCommand extends BaseCommand {
        parentaStaffUserId: Guid;
        externalId: string;
        constructor() {
            super('ParentaStaffUserIntegration/CreateFootstepsLoginIntegrationCommand');
        }
    }

    export class CreateDayshareLoginIntegrationCommand extends BaseCommand {
        parentaStaffUserId: Guid;
        externalId: string;
        constructor() {
            super('ParentaStaffUserIntegration/CreateDayshareLoginIntegrationCommand');
        }
    }
}
export namespace MailTrackerCommand {

    export class CreateMailTrackerCommand extends BaseCommand {
        messageId: string;
        product: string;
        area: string;
        nurseryId: string;
        childId: string;
        recipient: string;
        domain: string;
        event: string;
        messageHeaders: string;
        description: string;
        reason: string;
        error: string;
        timestamp: number;
        token: string;
        signature: string;
        contactId: string;
        constructor() {
            super('MailTracker/CreateMailTrackerCommand');
        }
    }
}
export namespace DiaryCommand {

    export class AddCustomDiaryEventToSeriesForBusinessCommand extends BaseCommand {
        businessId: Guid;
        customDiaryEventId: Guid;
        customDiaryEventSeriesId: Guid;
        constructor() {
            super('Diary/AddCustomDiaryEventToSeriesForBusinessCommand');
        }
    }

    export class CreateCustomDiaryEventSeriesWithEndDateCommand extends BaseCommand {
        subject: string;
        notes: string;
        diaryEventTypeId: Guid;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        rooms: string;
        userIds: any; // IEnumerable`1;
        whenToStartSeries: moment.Moment;
        whenToEndSeries: moment.Moment;
        recurrenceType: any; // RecurrenceType;
        legacyRef: string;
        constructor() {
            super('Diary/CreateCustomDiaryEventSeriesWithEndDateCommand');
        }
    }

    export class CreateCustomDiaryEventSeriesWithFixedNumberCommand extends BaseCommand {
        subject: string;
        notes: string;
        diaryEventTypeId: Guid;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        rooms: string;
        userIds: any; // IEnumerable`1;
        whenToStartSeries: moment.Moment;
        totalInSeries: number;
        recurrenceType: any; // RecurrenceType;
        legacyRef: string;
        constructor() {
            super('Diary/CreateCustomDiaryEventSeriesWithFixedNumberCommand');
        }
    }

    export class CreateCustomDiaryEventTypeCommand extends BaseCommand {
        name: string;
        constructor() {
            super('Diary/CreateCustomDiaryEventTypeCommand');
        }
    }

    export class CreateDiaryEventTypeCommand extends BaseCommand {
        diaryEventTypeId: Guid;
        name: string;
        constructor() {
            super('Diary/CreateDiaryEventTypeCommand');
        }
    }

    export class CreateEmptyCustomDiaryEventSeriesWithEndDateForBusinessCommand extends BaseCommand {
        businessId: Guid;
        whenToStartSeries: moment.Moment;
        whenToEndSeries: moment.Moment;
        recurrenceType: any; // RecurrenceType;
        legacyRef: string;
        constructor() {
            super('Diary/CreateEmptyCustomDiaryEventSeriesWithEndDateForBusinessCommand');
        }
    }

    export class CreateEmptyCustomDiaryEventSeriesWithFixedNumberForBusinessCommand extends BaseCommand {
        whenToStartSeries: moment.Moment;
        totalInSeries: number;
        recurrenceType: any; // RecurrenceType;
        legacyRef: string;
        businessId: Guid;
        constructor() {
            super('Diary/CreateEmptyCustomDiaryEventSeriesWithFixedNumberForBusinessCommand');
        }
    }

    export class CreateSingleCustomDiaryEventCommand extends BaseCommand {
        subject: string;
        notes: string;
        diaryEventTypeId: Guid;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        rooms: string;
        userIds: any; // IEnumerable`1;
        legacyRef: string;
        constructor() {
            super('Diary/CreateSingleCustomDiaryEventCommand');
        }
    }

    export class CreateSingleCustomDiaryEventForBusinessCommand extends BaseCommand {
        subject: string;
        notes: string;
        diaryEventTypeId: Guid;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        rooms: string;
        userIds: any; // IEnumerable`1;
        legacyRef: string;
        businessId: Guid;
        constructor() {
            super('Diary/CreateSingleCustomDiaryEventForBusinessCommand');
        }
    }

    export class DeleteCustomDiaryEventCommand extends BaseCommand {
        customDiaryEventId: Guid;
        constructor() {
            super('Diary/DeleteCustomDiaryEventCommand');
        }
    }

    export class DeleteCustomDiaryEventSeriesCommand extends BaseCommand {
        customDiaryEventSeriesId: Guid;
        constructor() {
            super('Diary/DeleteCustomDiaryEventSeriesCommand');
        }
    }

    export class DeleteCustomDiaryEventTypeCommand extends BaseCommand {
        customDiaryEventTypeId: Guid;
        constructor() {
            super('Diary/DeleteCustomDiaryEventTypeCommand');
        }
    }

    export class UpdatePartOfCustomDiaryEventSeriesWithEndDateCommand extends BaseCommand {
        subject: string;
        notes: string;
        diaryEventTypeId: Guid;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        rooms: string;
        userIds: any; // IEnumerable`1;
        customDiaryEventSeriesId: Guid;
        whenSeriesStarts: moment.Moment;
        whenSeriesEnds: moment.Moment;
        recurrenceType: any; // RecurrenceType;
        constructor() {
            super('Diary/UpdatePartOfCustomDiaryEventSeriesWithEndDateCommand');
        }
    }

    export class UpdateCustomDiaryEventSeriesWithFixedNumberCommand extends BaseCommand {
        subject: string;
        notes: string;
        diaryEventTypeId: Guid;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        rooms: string;
        userIds: any; // IEnumerable`1;
        customDiaryEventSeriesId: Guid;
        whenSeriesStarts: moment.Moment;
        totalInSeries: number;
        recurrenceType: any; // RecurrenceType;
        constructor() {
            super('Diary/UpdateCustomDiaryEventSeriesWithFixedNumberCommand');
        }
    }

    export class UpdateCustomDiaryEventSeriesWithEndDateCommand extends BaseCommand {
        subject: string;
        notes: string;
        diaryEventTypeId: Guid;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        rooms: string;
        userIds: any; // IEnumerable`1;
        customDiaryEventSeriesId: Guid;
        whenSeriesStarts: moment.Moment;
        whenSeriesEnds: moment.Moment;
        recurrenceType: any; // RecurrenceType;
        constructor() {
            super('Diary/UpdateCustomDiaryEventSeriesWithEndDateCommand');
        }
    }

    export class UpdatePartOfCustomDiaryEventSeriesWithFixedNumberCommand extends BaseCommand {
        subject: string;
        notes: string;
        diaryEventTypeId: Guid;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        rooms: string;
        userIds: any; // IEnumerable`1;
        customDiaryEventSeriesId: Guid;
        whenSeriesStarts: moment.Moment;
        totalInSeries: number;
        recurrenceType: any; // RecurrenceType;
        constructor() {
            super('Diary/UpdatePartOfCustomDiaryEventSeriesWithFixedNumberCommand');
        }
    }

    export class UpdateSingleCustomDiaryEventCommand extends BaseCommand {
        subject: string;
        notes: string;
        diaryEventTypeId: Guid;
        whenFrom: moment.Moment;
        whenTo: moment.Moment;
        rooms: string;
        userIds: any; // IEnumerable`1;
        customDiaryEventId: Guid;
        constructor() {
            super('Diary/UpdateSingleCustomDiaryEventCommand');
        }
    }
}
export namespace CarerCommand {

    export class MarkAllNotificationsAsReadCommand extends BaseCommand {
        constructor() {
            super('Carer/MarkAllNotificationsAsReadCommand');
        }
    }

    export class MarkNotificationAsReadCommand extends BaseCommand {
        notificationId: Guid;
        constructor() {
            super('Carer/MarkNotificationAsReadCommand');
        }
    }

    export class SendNotificationCommand extends BaseCommand {
        carerId: Guid;
        message: string;
        action: string;
        actionType: any; // ActionType;
        notificationType: any; // NotificationType;
        constructor() {
            super('Carer/SendNotificationCommand');
        }
    }
}
export namespace CarerPerferenceCommand {

    export class CreateObservationViewingCarerPreferenceCommand extends BaseCommand {
        data: string;
        constructor() {
            super('CarerPerference/CreateObservationViewingCarerPreferenceCommand');
        }
    }

    export class UpdateObservationViewingCarerPreferenceCommand extends BaseCommand {
        carerPreferenceId: Guid;
        data: string;
        constructor() {
            super('CarerPerference/UpdateObservationViewingCarerPreferenceCommand');
        }
    }

    export class UpdateChildrenColourCarerPreferenceCommand extends BaseCommand {
        carerPreferenceId: Guid;
        data: string;
        constructor() {
            super('CarerPerference/UpdateChildrenColourCarerPreferenceCommand');
        }
    }

    export class CreateChildrenColourCarerPreferenceCommand extends BaseCommand {
        data: string;
        constructor() {
            super('CarerPerference/CreateChildrenColourCarerPreferenceCommand');
        }
    }
}
export namespace CarerIntegrationCommand {

    export class CreateCalendarEndpointCommand extends BaseCommand {
        carerId: Guid;
        constructor() {
            super('CarerIntegration/CreateCalendarEndpointCommand');
        }
    }
}
export namespace BusinessCommand {

    export class DeactivateBusinessCommand extends BaseCommand {
        businessId: Guid;
        constructor() {
            super('Business/DeactivateBusinessCommand');
        }
    }

    export class ActivateBusinessCommand extends BaseCommand {
        businessId: Guid;
        constructor() {
            super('Business/ActivateBusinessCommand');
        }
    }

    export class CreateBusinessCommand extends BaseCommand {
        name: string;
        ownedSystems: any; // ParentaSystem;
        legacyId: number;
        emailAddress: string;
        constructor() {
            super('Business/CreateBusinessCommand');
        }
    }

    export class UpdateOwnedBusinessCommand extends BaseCommand {
        businessId: Guid;
        ownedParentaSystems: any; // ParentaSystem;
        constructor() {
            super('Business/UpdateOwnedBusinessCommand');
        }
    }
}
export namespace BusinessIntegrationCommand {

    export class CreateZendeskIntegrationCommand extends BaseCommand {
        businessId: Guid;
        externalId: string;
        constructor() {
            super('BusinessIntegration/CreateZendeskIntegrationCommand');
        }
    }
}
export namespace AuthCommand {

    export class AbacusLegacyLoginCommand extends BaseCommand {
        uniqueId: string;
        character1: string;
        characterPosition1: number;
        character2: string;
        characterPosition2: number;
        character3: string;
        characterPosition3: number;
        constructor() {
            super('Auth/AbacusLegacyLoginCommand');
        }
    }

    export class CarerLegacyLoginCommand extends BaseCommand {
        legacyBusinessId: number;
        dateOfBirth: string;
        password: string;
        username: string;
        constructor() {
            super('Auth/CarerLegacyLoginCommand');
        }
    }

    export class CheckPasswordCommand extends BaseCommand {
        password: string;
        constructor() {
            super('Auth/CheckPasswordCommand');
        }
    }

    export class CreateAuthAppicationCommand extends BaseCommand {
        userId: Guid;
        name: string;
        constructor() {
            super('Auth/CreateAuthAppicationCommand');
        }
    }

    export class LoginParentaStaffUserCommand extends BaseCommand {
        emailAddress: string;
        password: string;
        constructor() {
            super('Auth/LoginParentaStaffUserCommand');
        }
    }

    export class RenewTokenCommand extends BaseCommand {
        token: string;
        constructor() {
            super('Auth/RenewTokenCommand');
        }
    }
}
