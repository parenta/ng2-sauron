import { IBaseCommandResult } from './ibase-command-result';
export interface IExecutionResult<TBaseCommandResult extends IBaseCommandResult> {
    result: TBaseCommandResult;
    success: boolean;
    failMessage: string;
    errors: any[];
}
