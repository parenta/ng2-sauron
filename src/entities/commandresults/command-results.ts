﻿import { Guid } from '../../types/guid';
import { IBaseCommandResult } from './ibase-command-result';
import * as moment from 'moment';
export namespace ViewedSystemTourCommandResult {
   export interface IUserViewedWelcomeTourCommandResult extends IBaseCommandResult {
   }

}
export namespace UserCommandResult {
   export interface ICreateBasicUserCommandResult extends IBaseCommandResult {
      userId: Guid;
   }

   export interface ICreateNotificationForUserCommandResult extends IBaseCommandResult {
      notificationId: Guid;
   }

   export interface IMarkAllNotificationsForUserAsReadCommandResult extends IBaseCommandResult {
   }

   export interface IMarkNotificationAsReadCommandResult extends IBaseCommandResult {
      notificationId: Guid;
   }

   export interface IMarkSystemAlertAsClosedCommandResult extends IBaseCommandResult {
   }

   export interface IMarkSystemAlertAsSeenCommandResult extends IBaseCommandResult {
   }

   export interface IUpdateLegacyIdCommandResult extends IBaseCommandResult {
   }

}
export namespace UserPreferenceCommandResult {
   export interface ICreateDiarySettingsUserPreferenceCommandResult extends IBaseCommandResult {
   }

   export interface ICreateDashboardLayoutUserPreferenceCommandResult extends IBaseCommandResult {
   }

   export interface IUpdateDiarySettingsUserPreferenceCommandResult extends IBaseCommandResult {
   }

   export interface IUpdateDashboardLayoutUserPreferenceCommandResult extends IBaseCommandResult {
   }

}
export namespace UserIntegrationCommandResult {
   export interface ICreateCalendarEndpointCommandResult extends IBaseCommandResult {
      uniqueRef: string;
   }

}
export namespace CarerIntegrationCommandResult {
   export interface ICreateCalendarEndpointCommandResult extends IBaseCommandResult {
      uniqueRef: string;
   }

}
export namespace SystemMessageCommandResult {
   export interface ICreateGlobalSystemMessageCommandResult extends IBaseCommandResult {
   }

   export interface ICreateMultiBusinessSystemMessageCommandResult extends IBaseCommandResult {
   }

   export interface ICreateSingleBusinessSystemMessageCommandResult extends IBaseCommandResult {
   }

   export interface ICreateSingleUserSystemMessageCommandResult extends IBaseCommandResult {
   }

}
export namespace SystemAlertCommandResult {
   export interface ICreateGlobalSystemAlertCommandResult extends IBaseCommandResult {
   }

   export interface ICreateMultiBusinessSystemAlertCommandResult extends IBaseCommandResult {
   }

   export interface ICreateSingleUserSystemAlertCommandResult extends IBaseCommandResult {
   }

   export interface ICreateSingleBusinessSystemAlertCommandResult extends IBaseCommandResult {
   }

   export interface IRemoveSystemAlertCommandResult extends IBaseCommandResult {
   }

}
export namespace ParentaStaffUserCommandResult {
   export interface IChangeBasicDetailsCommandResult extends IBaseCommandResult {
   }

   export interface IChangeEmailCommandResult extends IBaseCommandResult {
   }

   export interface IChangePasswordCommandResult extends IBaseCommandResult {
   }

   export interface ICreateParentaStaffUserCommandResult extends IBaseCommandResult {
      userId: Guid;
   }

   export interface IOverrideBasicDetailsCommandResult extends IBaseCommandResult {
   }

   export interface IOverrideEmailAddressCommandResult extends IBaseCommandResult {
   }

   export interface IOverridePasswordCommandResult extends IBaseCommandResult {
   }

   export interface IResetPasswordCommandResult extends IBaseCommandResult {
   }

   export interface ISendPasswordResetTokenCommandResult extends IBaseCommandResult {
      token: string;
   }

   export interface IUpdateProfileImageUrlCommandResult extends IBaseCommandResult {
   }

}
export namespace ParentaStaffUserIntegrationCommandResult {
   export interface ICreateBreatheHrIntegrationCommandResult extends IBaseCommandResult {
   }

   export interface ICreateFootstepsLoginIntegrationCommandResult extends IBaseCommandResult {
   }

   export interface ICreateDayshareLoginIntegrationCommandResult extends IBaseCommandResult {
   }

   export interface IUpdateBreatheHrIntegrationCommandResult extends IBaseCommandResult {
   }

}
export namespace MailTrackerCommandResult {
   export interface ICreateMailTrackerCommandResult extends IBaseCommandResult {
   }

}
export namespace DiaryCommandResult {
   export interface IAddCustomDiaryEventToSeriesForBusinessCommandResult extends IBaseCommandResult {
   }

   export interface ICreateCustomDiaryEventSeriesWithEndDateCommandResult extends IBaseCommandResult {
   }

   export interface ICreateCustomDiaryEventSeriesWithFixedNumberCommandResult extends IBaseCommandResult {
   }

   export interface ICreateCustomDiaryEventTypeCommandResult extends IBaseCommandResult {
   }

   export interface ICreateDiaryEventTypeCommandResult extends IBaseCommandResult {
   }

   export interface ICreateEmptyCustomDiaryEventSeriesWithEndDateForBusinessCommandResult extends IBaseCommandResult {
      customDiaryEventSeriesId: Guid;
   }

   export interface ICreateEmptyCustomDiaryEventSeriesWithFixedNumberForBusinessCommandResult extends IBaseCommandResult {
      customDiaryEventSeriesId: Guid;
   }

   export interface ICreateSingleCustomDiaryEventCommandResult extends IBaseCommandResult {
      customDiaryEventId: Guid;
   }

   export interface ICreateSingleCustomDiaryEventForBusinessCommandResult extends IBaseCommandResult {
      customDiaryEventId: Guid;
   }

   export interface IDeleteCustomDiaryEventCommandResult extends IBaseCommandResult {
   }

   export interface IDeleteCustomDiaryEventSeriesCommandResult extends IBaseCommandResult {
   }

   export interface IDeleteCustomDiaryEventTypeCommandResult extends IBaseCommandResult {
   }

   export interface IUpdateCustomDiaryEventSeriesWithFixedNumberCommandResult extends IBaseCommandResult {
   }

   export interface IUpdateCustomDiaryEventSeriesWithEndDateCommandResult extends IBaseCommandResult {
   }

   export interface IUpdatePartOfCustomDiaryEventSeriesWithEndDateCommandResult extends IBaseCommandResult {
   }

   export interface IUpdatePartOfCustomDiaryEventSeriesWithFixedNumberCommandResult extends IBaseCommandResult {
   }

   export interface IUpdateSingleCustomDiaryEventCommandResult extends IBaseCommandResult {
   }

}
export namespace CarerCommandResult {
   export interface IMarkAllNotificationsAsReadCommandResult extends IBaseCommandResult {
   }

   export interface IMarkNotificationAsReadCommandResult extends IBaseCommandResult {
      notificationId: Guid;
   }

   export interface ISendNotificationCommandResult extends IBaseCommandResult {
      notificationId: Guid;
   }

}
export namespace CarerPerferenceCommandResult {
   export interface ICreateObservationViewingCarerPreferenceCommandResult extends IBaseCommandResult {
   }

   export interface ICreateChildrenColourCarerPreferenceCommandResult extends IBaseCommandResult {
   }

   export interface IUpdateObservationViewingCarerPreferenceCommandResult extends IBaseCommandResult {
   }

   export interface IUpdateChildrenColourCarerPreferenceCommandResult extends IBaseCommandResult {
   }

}
export namespace BusinessCommandResult {
   export interface IDeactivateBusinessCommandResult extends IBaseCommandResult {
   }

   export interface IActivateBusinessCommandResult extends IBaseCommandResult {
   }

   export interface ICreateBusinessCommandResult extends IBaseCommandResult {
      businessId: Guid;
   }

   export interface IUpdateOwnedBusinessCommandResult extends IBaseCommandResult {
   }

}
export namespace BusinessIntegrationCommandResult {
   export interface ICreateZendeskIntegrationCommandResult extends IBaseCommandResult {
      integrationId: Guid;
   }

}
export namespace AuthCommandResult {
   export interface IAbacusLegacyLoginCommandResult extends IBaseCommandResult {
      authToken: string;
      legacyId: number;
      permissions: any; // Permission;
      uniqueId: string;
      expireDate: moment.Moment;
      userId: Guid;
      emailAddress: string;
      name: string;
      nurseryName: string;
      businessId: Guid;
      businessLegacyId: number;
   }

   export interface ICarerLegacyLoginCommandResult extends IBaseCommandResult {
      legacyId: number;
      authToken: string;
      expireDate: moment.Moment;
      userId: Guid;
      name: string;
      businessId: Guid;
      businessLegacyId: number;
   }

   export interface ICheckPasswordCommandResult extends IBaseCommandResult {
      passwordMatch: boolean;
   }

   export interface ICreateAuthAppicationCommandResult extends IBaseCommandResult {
      apiKey: Guid;
   }

   export interface ILoginParentaStaffUserCommandResult extends IBaseCommandResult {
      userId: Guid;
      authToken: string;
      expireDate: moment.Moment;
      role: any; // Role;
   }

   export interface IRenewTokenCommandResult extends IBaseCommandResult {
      userId: Guid;
      authToken: string;
      expireDate: moment.Moment;
   }

}
