﻿import { Guid } from '../../types/guid';
import { IBaseQuery } from './ibase-query';
import * as moment from 'moment';
export namespace ViewedSystemTour {
   export interface IViewedSystemTour extends IBaseQuery {
      userId: Guid;
      systemTour: any; // SystemTour;
      whenEnded: moment.Moment;
      id: Guid;
   }
}
export namespace User {
   export interface IUser extends IBaseQuery {
      legacyId: number;
      firstName: string;
      lastName: string;
      emailAddress: string;
      businessId: Guid;
      id: Guid;
   }
   export interface IUsersWithIntegration extends IBaseQuery {
      userIntegrationId: Guid;
      userIntegrationType: any; // UserIntegrationType;
      externalId: string;
      id: Guid;
   }
}
export namespace UserPreference {
   export interface IUserPreference extends IBaseQuery {
      userId: Guid;
      data: string;
      systemPreference: any; // SystemPreference;
      id: Guid;
   }
}
export namespace CarerPreference {
   export interface ICarerPreference extends IBaseQuery {
      carerId: Guid;
      data: string;
      systemPreference: any; // SystemPreference;
      id: Guid;
   }
}
export namespace SystemMessage {
   export interface ISystemMessage extends IBaseQuery {
      message: string;
      colourType: any; // ColourType;
      senderFirstName: string;
      senderLastName: string;
      id: Guid;
   }
}
export namespace SystemAlert {
   export interface IActiveSystemAlert extends IBaseQuery {
      subject: string;
      message: string;
      distributionType: any; // DistributionType;
      colourType: any; // ColourType;
      isClosable: boolean;
      whenCreated: moment.Moment;
      distributedTo: string;
      id: Guid;
   }
   export interface IUnreadOpenSystemAlert extends IBaseQuery {
      subject: string;
      message: string;
      distributionType: any; // DistributionType;
      colourType: any; // ColourType;
      isClosable: boolean;
      userId: Guid;
      whenAlerted: any; // Nullable`1;
      userSystemAlertId: Guid;
      id: Guid;
   }
}
export namespace ParentaStaffUser {
   export interface IParentaStaffUser extends IBaseQuery {
      firstName: string;
      lastName: string;
      emailAddress: string;
      isConfirmed: boolean;
      role: any; // Role;
      id: Guid;
   }
}
export namespace ParentaStaffUserIntegration {
   export interface IParentaStaffUserIntegration extends IBaseQuery {
      userId: Guid;
      externalId: string;
      parentaStaffUserIntegrationType: any; // ParentaStaffUserIntegrationType;
      id: Guid;
   }
}
export namespace Notification {
   export interface IAllNotifications extends IBaseQuery {
      userId: Guid;
      message: string;
      action: string;
      actionType: any; // ActionType;
      whenNotified: moment.Moment;
      whenRead: moment.Moment;
      notificationType: any; // NotificationType;
      id: Guid;
   }
   export interface IUnreadNotifications extends IBaseQuery {
      userId: Guid;
      message: string;
      action: string;
      actionType: any; // ActionType;
      whenNotified: moment.Moment;
      whenRead: any; // Nullable`1;
      notificationType: any; // NotificationType;
      id: Guid;
   }
}
export namespace MailTracker {
   export interface IFailedBusiness extends IBaseQuery {
      nurseryId: string;
      minTimestamp: moment.Moment;
      maxTimestamp: moment.Moment;
      id: Guid;
   }
   export interface IMailTracker extends IBaseQuery {
      messageId: string;
      product: number;
      area: number;
      nurseryId: string;
      childId: string;
      contactId: string;
      recipient: string;
      domain: string;
      description: string;
      reason: string;
      mailEvent: number;
      error: string;
      timestamp: moment.Moment;
      token: string;
      id: Guid;
   }
}
export namespace Diary {
   export interface IAttachedUser extends IBaseQuery {
      businessId: Guid;
      firstName: string;
      lastName: string;
      customDiaryEventId: Guid;
      id: Guid;
   }
   export interface ICustomDiaryEvent extends IBaseQuery {
      subject: string;
      notes: string;
      diaryEventTypeId: Guid;
      whenFrom: moment.Moment;
      whenTo: moment.Moment;
      rooms: string;
      businessId: Guid;
      customDiaryEventSeriesId: any; // Nullable`1;
      numberOfUsers: number;
      legacyRef: string;
      id: Guid;
   }
   export interface ICustomDiaryEventSeries extends IBaseQuery {
      whenFrom: moment.Moment;
      whenTo: moment.Moment;
      recurrenceType: any; // RecurrenceType;
      totalInSeries: any; // Nullable`1;
      businessId: Guid;
      legacyRef: string;
      id: Guid;
   }
   export interface IDiaryEventType extends IBaseQuery {
      name: string;
      businessId: any; // Nullable`1;
      isDeletable: boolean;
      whenDeleted: any; // Nullable`1;
      id: Guid;
   }
}
export namespace Carer {
   export interface ICarer extends IBaseQuery {
      legacyRef: string;
      businessId: Guid;
      id: Guid;
   }
}
export namespace Business {
   export interface IBusiness extends IBaseQuery {
      legacyId: any; // Nullable`1;
      name: string;
      ownedSystems: any; // ParentaSystem;
      emailAddress: string;
      isActive: boolean;
      id: Guid;
   }
}
export namespace BusinessIntegration {
   export interface IBusinessIntegration extends IBaseQuery {
      businessId: Guid;
      externalId: string;
      businessIntegrationType: any; // BusinessIntegrationType;
      id: Guid;
   }
}
export namespace Auth {
   export interface IAuthApplication extends IBaseQuery {
      userId: Guid;
      name: string;
      whenApplied: moment.Moment;
      apiKey: Guid;
      hash: string;
      isActive: boolean;
      id: Guid;
   }
   export interface IAuthToken extends IBaseQuery {
      token: string;
      whenExpires: moment.Moment;
      whenCreated: moment.Moment;
      userId: Guid;
      authApplicationId: Guid;
      apiKey: Guid;
      id: Guid;
   }
}
