import { Injectable, Inject } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {BaseCommand} from './entities/commands/base-command';
import {IBaseCommandResult} from './entities/commandresults/ibase-command-result';
import {IExecutionResult} from './entities/commandresults/iexecution-result';
import {Observable} from 'rxjs';
import {ISauronClientConfig} from './config/isauron-client-config';
import {SAURON_CLIENT_CONFIG} from './config/sauron-client-config';
import {Guid} from './types/guid';
import {AuthenticationService} from './authentication.service';

@Injectable()
export class ExecutionService {

    constructor(
        @Inject(SAURON_CLIENT_CONFIG)private config: ISauronClientConfig,
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

    executeCommand<TBaseCommand extends BaseCommand, TBaseCommandResult extends IBaseCommandResult>(cmd: TBaseCommand):
        Observable<IExecutionResult<TBaseCommandResult>> {
        if (this.authenticationService.newTokenNeeded()) {
            return this.authenticationService.getNewToken(() => this.execute<TBaseCommand, TBaseCommandResult>(cmd));
        } else {
            return this.execute<TBaseCommand, TBaseCommandResult>(cmd);
        }
    }

    private execute<TBaseCommand extends BaseCommand, TBaseCommandResult extends IBaseCommandResult>(cmd: TBaseCommand):
        Observable<IExecutionResult<TBaseCommandResult>> {
        let that = this;
        let authKey = this.authenticationService.generateAuthKey();
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Token ' + authKey,
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.generateCommandUrl(cmd), cmd, options)
            .map(function(res: Response){
                let body = res.json();
                return body || { };
            }).catch(function(error: any) {
                if (that.config.errorFunc) {
                    that.config.errorFunc(error);
                }
                let errMsg = (error.message) ? error.message :
                    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });
    }

    private generateCommandUrl(command: BaseCommand): string {
        return this.config.apiUrl
            + Guid.newGuid() + '/execute/'
            + command.getRoute();
    }
}
