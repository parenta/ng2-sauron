import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Guid } from './types/guid';
import {SAURON_CLIENT_CONFIG} from './config/sauron-client-config';
import {ISauronClientConfig} from './config/isauron-client-config';
import { LocalStorageService } from 'angular-2-local-storage/index';
import * as moment from 'moment';
import { AuthCommand } from './entities/commands/commands';
import { AuthCommandResult } from './entities/commandresults/command-results';
import { BaseCommand } from './entities/commands/base-command';
import { IExecutionResult } from './entities/commandresults/iexecution-result';
import {Observable} from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';


@Injectable()
export class AuthenticationService {

    get token(): string {
        return <string>this.localStorageService.get('token');
    }
    set token(value: string) {
        this.localStorageService.set('token', value);
    }

    get expiryDate(): moment.Moment {
        return <moment.Moment>this.localStorageService.get('expiryDate');
    }
    set expiryDate(value: moment.Moment) {
        this.localStorageService.set('expiryDate', value);
    }

     get userId(): Guid {
         return <Guid>this.localStorageService.get('userId');
     }
     set userId(value: Guid) {
         this.localStorageService.set('userId', value);
     }

    get businessId(): Guid {
        return <Guid>this.localStorageService.get('businessId');
    }
    set businessId(value: Guid) {
        this.localStorageService.set('businessId', value);
    }

    get businessLegacyId(): number {
        return <number>this.localStorageService.get('businessLegacyId');
    }
    set businessLegacyId(value: number) {
        this.localStorageService.set('businessLegacyId', value);
    }

    get legacyId(): number {
        return <number>this.localStorageService.get('legacyId');
    }
    set legacyId(value: number) {
        this.localStorageService.set('legacyId', value);
    }

    get name(): string {
        return <string>this.localStorageService.get('name');
    }
    set name(value: string) {
        this.localStorageService.set('name', value);
    }

    constructor(
        @Inject(SAURON_CLIENT_CONFIG)private config: ISauronClientConfig,
        private http: Http,
        private localStorageService: LocalStorageService) {

    }

    generateAuthKey(): string {
        let authKey: string;
        if (this.expiryDate && moment(this.expiryDate).utc() > moment.utc()) {
            authKey = btoa(JSON.stringify( { apiKey : this.config.apiKey, token : this.token}));
        } else {
            authKey = btoa(JSON.stringify( { apiKey : this.config.apiKey, token : ''}));
        }
        return authKey;
    }

    newTokenNeeded(): boolean {
        if (!this.token) {
            return false;
        }
        if (this.expiryDate && moment(this.expiryDate).utc() > moment.utc()) {
            return false;
        }
        return true;
    }

    getNewToken(outFunc: () => any): any {
        let that = this;
        let cmd: AuthCommand.RenewTokenCommand = new AuthCommand.RenewTokenCommand();
        cmd.token = this.token;

        let headers = new Headers({
            'Authorization' : 'Token ' + this.generateAuthKey(),
            'Content-Type' : 'application/json',
            'Accept': 'q=0.8;application/json;q=0.9'
        });

        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.generateCommandUrl(cmd), cmd, options)
            .map(function(res: Response) {
                let body = <IExecutionResult<AuthCommandResult.IRenewTokenCommandResult>>res.json();
                that.processRenewTokenResult(body.result);
                return outFunc();
            })
            .catch(function (error: any) {
                let errMsg = (error.message) ? error.message :
                    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });
    }

    loginParentaStaffUser(emailAddress: string, password: string): Observable<boolean> {
        console.log('loginParentaStaffUser');
        console.log(this.generateAuthKey());
        let that = this;
        let cmd: AuthCommand.LoginParentaStaffUserCommand = new AuthCommand.LoginParentaStaffUserCommand();
        cmd.emailAddress = emailAddress;
        cmd.password = password;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Token ' + this.generateAuthKey(),
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        let options = new RequestOptions({ headers: headers });
        console.log(this.generateCommandUrl(cmd));
        return this.http.post(this.generateCommandUrl(cmd), cmd, options)
            .map(function(res: Response) {
                let body = res.json();
                console.log(res);
                console.log(body);
                if (!body.success) {
                    return false;
                }

                that.processParentaStaffUserLoginResult(body.result);
                return true;
            })
            .catch(function(error: any) {
                if (that.config.errorFunc) {
                    that.config.errorFunc(error);
                }
                let errMsg = (error.message) ? error.message :
                    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });
    }

    loginLegacyAbacusUser(uniqueId: string, characterPosition1: number, character1: string, characterPosition2: number,
                          character2: string, characterPosition3: number, character3: string): Observable<boolean> {
        let that = this;
        let cmd: AuthCommand.AbacusLegacyLoginCommand = new AuthCommand.AbacusLegacyLoginCommand();
        cmd.uniqueId = uniqueId;
        cmd.characterPosition1 = characterPosition1;
        cmd.character1 = character1;
        cmd.characterPosition2 = characterPosition2;
        cmd.character2 = character2;
        cmd.characterPosition3 = characterPosition3;
        cmd.character3 = character3;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Token ' + this.generateAuthKey(),
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.generateCommandUrl(cmd), cmd, options)
            .map(function(res: Response) {
                let body = res.json();
                if (!body.success) {
                    return false;
                }
                that.processAbacusUserLoginResult(body.result);
                return true;
            })
            .catch(function(error: any) {
                if (that.config.errorFunc) {
                    that.config.errorFunc(error);
                }
                let errMsg = (error.message) ? error.message :
                    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });
    }

    loginLegacyCarer(username: string, password: string, legacyBusinessId: number, dateOfBirth: string): Observable<boolean> {
        let that = this;
        let cmd: AuthCommand.CarerLegacyLoginCommand = new AuthCommand.CarerLegacyLoginCommand();
        cmd.username = username;
        cmd.password = password;
        cmd.legacyBusinessId = legacyBusinessId;
        cmd.dateOfBirth = dateOfBirth;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Token ' + this.generateAuthKey(),
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.generateCommandUrl(cmd), cmd, options)
            .map(function(res: Response) {
                let body = res.json();
                if (!body.success) {
                    return false;
                }
                that.processCarerLegacyLoginResult(body.result);
                return true;
            })
            .catch(function(error: any) {
                if (that.config.errorFunc) {
                    that.config.errorFunc(error);
                }
                let errMsg = (error.message) ? error.message :
                    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });
    }

    private processParentaStaffUserLoginResult(result: AuthCommandResult.ILoginParentaStaffUserCommandResult): void {
        this.token = result.authToken;
        this.expiryDate = result.expireDate;
        this.userId = result.userId;

        // this.localStorageService.set('token', this.token);
        // this.localStorageService.set('expiryDate', this.expiryDate);
        // this.localStorageService.set('userId', this.userId);
    }

    private processRenewTokenResult(data: AuthCommandResult.IRenewTokenCommandResult): void {
        this.token = data.authToken;
        this.expiryDate = data.expireDate;
        this.userId = data.userId;
    }
    private processAbacusUserLoginResult(data: AuthCommandResult.IAbacusLegacyLoginCommandResult): void {
        this.token = data.authToken;
        this.expiryDate = data.expireDate;
        this.userId = data.userId;
    }

    private processCarerLegacyLoginResult(data: AuthCommandResult.ICarerLegacyLoginCommandResult): void {
        this.token = data.authToken;
        this.expiryDate = data.expireDate;
        this.userId = data.userId;
        this.businessId = data.businessId;
        this.businessLegacyId = data.businessLegacyId;
        this.legacyId = data.legacyId;
        this.name = data.name;
    }

    private generateCommandUrl(command: BaseCommand): string {
        return this.config.apiUrl + Guid.newGuid() + '/execute/' + command.getRoute();
    }

}
