import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { IBaseQuery } from '../entities/queries/ibase-query';
import {Observable} from 'rxjs';
import { IApiQueryResponse } from '../entities/queries/iapi-query-response';
import { Guid } from '../types/guid';
import {SAURON_CLIENT_CONFIG} from '../config/sauron-client-config';
import {AuthenticationService} from '../authentication.service';
import {ISauronClientConfig} from '../config/isauron-client-config';

export abstract class BaseQueryService {

  constructor(
    private http: Http,
    private config: ISauronClientConfig,
    private authenticationService: AuthenticationService) {

  }

  protected generateReadUrl(namespace: string, entity: string): string {
    let url = this.config.apiUrl
      + Guid.newGuid() + '/read/' + namespace + '/'
      + entity;
    return url;
  }

  public runQuery<TReadEntity extends IBaseQuery>(query: jo): Observable<IApiQueryResponse<TReadEntity>> {
    if (this.authenticationService.newTokenNeeded()) {
      return this.authenticationService.getNewToken(() => this.query<TReadEntity>(query));
    }
    return this.query<TReadEntity>(query);
  }

  private query<TReadEntity extends IBaseQuery>(query: jo): Observable<IApiQueryResponse<TReadEntity>> {
    let that = this;
    let authKey = this.authenticationService.generateAuthKey();
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization' : 'Token ' + authKey,
      'Accept': 'q=0.8;application/json;q=0.9'
    });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(query.toString(), options).map(function (res: Response) {
        let body = res.json();
        return body || { };
      }).catch(function(error: any) {
        if (that.config.errorFunc) {
          that.config.errorFunc(error);
        }
        let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
      });
  }
}
