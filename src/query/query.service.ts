﻿import { Guid } from '../types/guid';
import * as moment from 'moment';
import { BaseQueryService } from './base-query-service';
import { Http } from '@angular/http';
import { AuthenticationService } from '../authentication.service';
import { Injectable, Inject } from '@angular/core';
import { SAURON_CLIENT_CONFIG } from '../config/sauron-client-config';
import { ISauronClientConfig } from '../config/isauron-client-config';
@Injectable()
export class ViewedSystemTourQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createViewedSystemTourQuery(): jo {
        return new jo(this.generateReadUrl('ViewedSystemTour', 'ViewedSystemTour'));
    }
}
@Injectable()
export class UserQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createUserQuery(): jo {
        return new jo(this.generateReadUrl('User', 'User'));
    }
    createUsersWithIntegrationQuery(): jo {
        return new jo(this.generateReadUrl('User', 'UsersWithIntegration'));
    }
}
@Injectable()
export class UserPreferenceQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createUserPreferenceQuery(): jo {
        return new jo(this.generateReadUrl('UserPreference', 'UserPreference'));
    }
}
@Injectable()
export class CarerPreferenceQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createCarerPreferenceQuery(): jo {
        return new jo(this.generateReadUrl('CarerPreference', 'CarerPreference'));
    }
}
@Injectable()
export class SystemMessageQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createSystemMessageQuery(): jo {
        return new jo(this.generateReadUrl('SystemMessage', 'SystemMessage'));
    }
}
@Injectable()
export class SystemAlertQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createActiveSystemAlertQuery(): jo {
        return new jo(this.generateReadUrl('SystemAlert', 'ActiveSystemAlert'));
    }
    createUnreadOpenSystemAlertQuery(): jo {
        return new jo(this.generateReadUrl('SystemAlert', 'UnreadOpenSystemAlert'));
    }
}
@Injectable()
export class ParentaStaffUserQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createParentaStaffUserQuery(): jo {
        return new jo(this.generateReadUrl('ParentaStaffUser', 'ParentaStaffUser'));
    }
}
@Injectable()
export class ParentaStaffUserIntegrationQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createParentaStaffUserIntegrationQuery(): jo {
        return new jo(this.generateReadUrl('ParentaStaffUserIntegration', 'ParentaStaffUserIntegration'));
    }
}
@Injectable()
export class NotificationQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createAllNotificationsQuery(): jo {
        return new jo(this.generateReadUrl('Notification', 'AllNotifications'));
    }
    createUnreadNotificationsQuery(): jo {
        return new jo(this.generateReadUrl('Notification', 'UnreadNotifications'));
    }
}
@Injectable()
export class MailTrackerQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createFailedBusinessQuery(): jo {
        return new jo(this.generateReadUrl('MailTracker', 'FailedBusiness'));
    }
    createMailTrackerQuery(): jo {
        return new jo(this.generateReadUrl('MailTracker', 'MailTracker'));
    }
}
@Injectable()
export class DiaryQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createAttachedUserQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'AttachedUser'));
    }
    createCustomDiaryEventQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'CustomDiaryEvent'));
    }
    createCustomDiaryEventSeriesQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'CustomDiaryEventSeries'));
    }
    createDiaryEventTypeQuery(): jo {
        return new jo(this.generateReadUrl('Diary', 'DiaryEventType'));
    }
}
@Injectable()
export class CarerQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createCarerQuery(): jo {
        return new jo(this.generateReadUrl('Carer', 'Carer'));
    }
}
@Injectable()
export class BusinessQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createBusinessQuery(): jo {
        return new jo(this.generateReadUrl('Business', 'Business'));
    }
}
@Injectable()
export class BusinessIntegrationQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createBusinessIntegrationQuery(): jo {
        return new jo(this.generateReadUrl('BusinessIntegration', 'BusinessIntegration'));
    }
}
@Injectable()
export class AuthQueryService extends BaseQueryService {
    constructor(http: Http, @Inject(SAURON_CLIENT_CONFIG)config: ISauronClientConfig, authenticationService: AuthenticationService) {
        super(http, config, authenticationService);
    }
    createAuthApplicationQuery(): jo {
        return new jo(this.generateReadUrl('Auth', 'AuthApplication'));
    }
    createAuthTokenQuery(): jo {
        return new jo(this.generateReadUrl('Auth', 'AuthToken'));
    }
}

