import { OpaqueToken } from '@angular/core';

export const SAURON_CLIENT_CONFIG = new OpaqueToken('SAURON_CLIENT_CONFIG');
