export interface ISauronClientConfig {
    apiUrl: string;
    apiKey: string;
    errorFunc: Function;
}
